var MagicPromise = function(fn){
    var value;
    var status = 'pending';
    var handlers = [];

    var _resolve = function(v){
        value = v;
        status = 'resolved';
        handlers.forEach(thenHandler);
        handlers = null;
    }
    var _reject = function(v){
        value = v;
        status = 'rejected';
        handlers.forEach(thenHandler);
        handlers = null;
    }
    var thenHandler = function(handle){
        if(status == 'pending') {
            handlers.push(handle);
        }else {
            if(status == 'resolved'){
                handle.resolve(value);
            }
            else{
                handle.reject(value);
            }
        }
    }
    this.then = function(resolved,rejected){
        return new MagicPromise(function(resolve,reject){

                thenHandler({
                    resolve: function(v){
                        if(typeof resolved == 'function')
                        return resolve(resolved(v));
                        else
                        return resolve(v);
                    },
                    reject: function(v){
                        if(typeof rejected == 'function')
                        return reject(rejected(v));
                        else
                        return reject(v);
                    }
                });

        });
    }
    fn(_resolve,_reject);
}

var M = new MagicPromise(function(res,rej){
    setTimeout(function(){
        res("resolved")
    },3000);
}).then(function(res){
    return "resolved again";
});
